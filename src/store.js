import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const state = {
  breeds:{},
  loading:false
}

const getters = {
  getBreeds(Store) {
    return Store.breeds
  }
}

const mutations = {
  setBreeds(state, breeds) {
    state.breeds = breeds
  }
}

const actions = {
  async loadBreeds(context) {
    try {
      const res = await axios.get('breeds/list/all');
      context.commit('setBreeds', res.data.message);
    } catch (e) {
      alert('error');
        } finally {
      this.loading = false
    }
  } 
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})
