import Vue from 'vue'
import App from './App.vue'
import router from './router';
import store from './store';
import LayoutDefault from './layouts/LayoutDefault.vue'
import vuetify from './plugins/vuetify';
import "./plugins/filters"
import axios from 'axios'

axios.defaults.baseURL = 'https://dog.ceo/api/';
Vue.prototype.$axios = axios

Vue.component('layout-default', LayoutDefault)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
