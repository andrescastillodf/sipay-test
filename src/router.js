import Vue from 'vue';
import Router from 'vue-router';

const Breed = () => import(`./pages/Breed.vue`);
const Breeds = () => import(`./pages/Breeds.vue`);
const PageNotFound = () => import(`./pages/PageNotFound.vue`);

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: `/`,
      name: `home-breeds`,
      component: Breeds,
    },
    {
      path: `/breeds/:id?`,
      name: `breeds`,
      component: Breeds,
    },
    {
      path: `/breed/:id/:subid?`,
      name: `breed`,
      component: Breed,
    },
    { path: "*", component: PageNotFound }
  ],
  mode: `history`,
});
